# Spiele programmieren mit Groovy

## Spiele entwickeln

Wir lernen zu programmieren um zu automatisieren - und lästige Aufgaben nicht mehr von Hand erledigen zu müssen. Das kann auch Aufgaben aus dem Schulalltag einschließen ;-). Die Programmierung ist nur ein Werkzeug. Nebenbei lernen wir in der Informatik auch, es effizient und effektiv einzusetzen.

In diesem Kurs entwickeln wir eine Wirtschaftssimulation und ein Adventure und finden dabei einen Einstieg in Datenstrukturen, Modellbildung und Programmierung. Das Ergebnis des ersten Teils/Halbjahres ist ein Programm das am PC in Eingabeaufforderung/Terminal läuft. Im zweiten Teil wird das gleiche Programm als App für Mobilgeräte umgesetzt nach dem Muster Model-View-Controller.

### Wirtschaftssimulation

Das erste Programm wird ein Spiel zum Handeln mit Waren, bei dem man nach X Tagen so viel Geld wie möglich verdienen muss.

* Kaufen und Verkaufen von Waren
* Schwankungen der Preise
* Mehrere Orte zum Handeln
* Ziel: Nach X Tagen max. Profit

![Marktplatz](bilder/marktplatz.jpg)

### Adventure

Als zweies Programm wir ein Adventure entwickelt, bei dem man sich durch verschiedene Szenen bewegt, Gegenstände sammeln und Aufgaben lösen muss.

* Bewegen zwischen Szenen im Raster
* Sammeln von Gegenständen
* Lösen von Aufgaben
* Ziel: Eine (sehr!) kurze Geschichte erfolgreich lösen

![Karte](bilder/karte.jpg)

### Weitere Beispiele/Aufgaben

* Zahlenraten
* [Türme von Hanoi](https://gitlab.com/axel-klinger/groovy-hanoi)
* [TicTacToe](https://gitlab.com/axel-klinger/groovy-tictactoe)
* [Galgenraten](https://gitlab.com/axel-klinger/groovy-hangman)
* [Adventure](https://gitlab.com/axel-klinger/groovy-adventure)
* [Kartensppiel](https://gitlab.com/axel-klinger/groovy-karten)
* [Wirtschaftssimulation](https://gitlab.com/axel-klinger/groovy-handel)


## Aufsetzen der Entwicklungsumgebung  für PC Programme

Unsere Entwicklungsumgebung besteht aus den Programmiersprachen Java und Groovy, der Versionsverwaltung Git und einem Editor für die Bearbeitung der Quelltexte. Als Test schreiben wir anschließend ein einzeliges Programm und versionieren es.

Bei der Installation und auch später bei der Programmierung werden wir immer wieder Befehle auf der Kommandozeile ausführen - auch später, wenn wir Programme für Mobilgeräte bauen. Daher ist es wichtig, dass du die Grundlagen der Kommandozeile kennst. Falls du dich mit der Kommandozeile deines Betriebssystems noch nicht auskennst, solltest du dir hier die Einführungseinheit zu [Windows](), [Linux]() und [Mac OS]() ansehen und die Beispiele selbst ausführen.

***Die Links zu den Dokumentationen solltest du dir zum Nachschlagen in den Favoriten deines Browsers speichern!***

### Installation von Git unter Windows, Linux, Mac OS

Mit der Versionsverwaltung git haben wir die Möglichkeit Änderungen an unseren Projekten zu verisonieren und somit bei Fehlern wieder auf einen funktionierenden Stand zurückzukehren.

[Download Git](https://git-scm.com/downloads)

Die grundlegenden Befehle zu Git werde ich im Laufe des Kurses zeigen und erklären. Weitere Informationen findest du in der [Dokumentation Git](https://git-scm.com/book/en/v2)

### Installation von Groovy unter Windows, Linux, Mac OS

Der einfachste Weg ist die Installation mit Hilfe des Software Development Kit Managers [SDKMAN](http://groovy-lang.org/install.html#SDKMAN).

Wir werden im Laufe des Kurses die wesentlichen Grundlagen von Groovy kennen lernen. weitere Informationen findest du in der [Dokumentation Groovy](http://groovy-lang.org/documentation.html)

### Installation des Editors Atom unter Windows, Linux, Mac OS

Atom ist ein sehr leistungsfähiger Editor für verschiedene Programmiersprachen und unterstützt ab der Version 2 (noch Beta, aber schon gut) auch die Versionsverwaltung Git.

[Download Atom](https://atom.io/beta)

### Testen der Entwicklungsumgebung

[![Entwicklungsumgebung mit Groovy/Git testen](https://img.youtube.com/vi/fbZOii_l7M4/maxresdefault.jpg)](https://youtu.be/fbZOii_l7M4)

Wenn das alles geht, dann ist die erste Einheit erfolgreich abgeschlossen!


## Erste Schritte in der Programmierung

### Eingabe, Variable und Ausgabe

In der ersten Einheit lernen wir Daten einzugeben, in Variablen zu speichern und Werte auszugeben.

### Der erste Warenkord

Mit dem folgenden Programm können wir bereits ein paar Artikel in einen Warenkorb legen. Die einzelnen Schritte können in github nachvollzogen werden. Zu jedem commit gibt es ein kurzes Video.

```groovy
// Angebot an Artikeln
def angebot = ['Honig', 'Salz', 'Tran', 'Pelze'] as Set
def get = { it -> System.console().readLine(it + '\n') }

def artikel = [] as Set

// Schleife mit Abbruchbedingung
while (artikel.size() < 3 ) {
  // Validierung der Auswahl
  def auswahl = get( 'Wähle ein Produkt aus ' + angebot)
  if ( angebot.contains(auswahl) ) {
    artikel.add( auswahl )
  } else {
    println auswahl + ' ist nicht im Angebot!'
  }
}

println artikel
```

// Alle Beispiele zum Nachlesen | als Video mit Erläuterung | ... ?

### Was haben wir bis hier kennen gelernt?

* Ein- und Ausgabe
* Schleifen
* Verzweigungen
* Zeichenketten
* Mengen



## Grundlagen der Programmierung

### Ein- und Ausgabe

### Variablen

Zahlen, Zeichenketten und Wahrheitswerte

### Schleifen

Alle Elemente einer Menge, feste Anzahl Wiederholungen und bedingte Ausführungen

### Verzweigungen

Wenn-Dann-Sonst (if else) und nach Auswahl (switch)

### Mengen und Listen

...

### Funktionen

...


## Modell der Wirtschaftssimulation

Bevor wir jetzt voreilig weiter in die Programmierung einsteigen, wollen wir erstmal nochmal einen Schritt zurückgehen und definieren, was wir eigentlich erreichen wollen. Wir beschreiben zunächst formlos was wir erreichen wollen, machen uns anschließen ein Modell davon und setzen es dann mit den Werkzeugen der Programmierung um.

[![Modell der Wirtschaftssimulation Video](https://img.youtube.com/vi/vLI5kN-_654/maxresdefault.jpg)](https://youtu.be/vLI5kN-_654)

### Marktplatz

Der Marktplatz zeigt die Produkte mit Anzahl und Preis, wie sie gekauft werden können.

```
Marktplatz 1
------------
1. Ware 1 - 20 Stk. - 13 EUR
2. Ware 2 - 40 Stk. - 27 EUR
3. Ware 3 - 10 Stk. - 17 EUR
```

### Tasche

In der Tasche befinden sich deine gekauften Waren. Die Tasche kann maximal X Waren aufnehmen.

### Waren

Die Waren sollten am Ende konfigurierbar sein, so dass man mit verschiedenen Konfigurationen mit anderen Waren handeln kann. Waren können hier zum Beispiel auch Aktien sein - und wer mehr lernen will, kann das ganze noch um eine Echtzeit-Abfrage von Werten aus dem Netz erweitern.

Beispiel für eine Konfiguration, wie sie aufgebaut sein könnte
```
[Marktplätze]
Frankfurt
London
New York
Tokio

[Waren]
Orangen 3-6 EUR
Äpfel   2-4 EUR
Pfeffer 10-20 EUR
```


## Modell des Adventures

Was brauchen wir im Adventure?

* Szenen im Raster angeordnet
* Gegenstände
* Aufgaben
* Handlung

### Szene

-> Objekt
-> Matrix

### Gegenstand

-> Objekt

### Aufgabe

-> Funktion

### Handlung

-> Graph


## Aufgaben

### Aufgaben und Lösungen

Die folgenden Aufgaben helfen dir, dein Verständnis für die Programmierung zu festigen. Versuche auch die einzelnen Beispiele zu verändern und ggf. andeere Lösungen zu finden.

### Eingabe und Ausgabe
[[
***Aufgage 1***

Schreibe ein Programm, das nach deinem Namen fragt und dich anschließend mit deinem Namen begrüßt.
]]((
  [![Modell der Wirtschaftssimulation Video](https://img.youtube.com/vi/CiXPG5JTQk0/maxresdefault.jpg)](https://youtu.be/CiXPG5JTQk0)
))

### Zahlen und Zeichenketten

Speichern und verändern von Werten.

[[
***Aufgabe 2***

Schreibe ein Programm, das zwei Zahlen einliest und anschließend Summe, Produkt, Differenz und Quotient der beiden Zahlen ausgibt.
]]((
```
def ersteZahl = System.console().readLine('Gib eine ganze Zahl ein!')
def zweiteZahl = System.console().readLine('Gib eine weitere ganze Zahl ein!')
println 'Die Summe von ' + ersteZahl + ' und ' + zweiteZahl + ' ist ' + (ersteZahl + zweiteZahl)
```
))

### Schleifen und Verzweigungen

[[
***Aufgabe 2***

Schreibe ein Programm mit einer Schleife, das deinen Namen 5 mal ausgibt.
]]((
```
5.times{
  println "Mein name"
}
```
  ))


### Mengen und Listen

### Klassen und Objekte

Beschreibung komplexer Objekte der realen Welt mit Klassen und Objekten.
